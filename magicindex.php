<?php
$mysqlhost = $_POST['mysqlhost'];
$mysqlrootuser = $_POST['mysqlrootuser'];
$mysqlrootpass = $_POST['mysqlrootpass'];
$mysqlsiteuser = $_POST['mysqlsiteuser'];
$mysqlsitepass = $_POST['mysqlsitepass'];
$database = $_POST['database'];
$makefile = $_POST['makefile'];
$sitename = $_POST['sitename'];
$vhostname = $_POST['vhostname'];
$drushinstall = "drush si -y --db-url=mysql://" . $mysqlsiteuser . ":" . $mysqlsitepass . "@" . $mysqlhost . "/" . $database . " --site-name=" . $sitename;

mkdir('/var/www/magic_hat/rabbit', 0777);

$connect_mysql = mysql_connect($mysqlhost, $mysqlrootuser, $mysqlrootpass);
if (!$connect_mysql)
{
  die('Could not connect to MySQL: ' . mysql_error());
}
$grant_privileges = mysql_query("GRANT SELECT, INSERT, UPDATE, DELETE, CREATE, DROP, INDEX, ALTER, CREATE TEMPORARY TABLES, LOCK TABLES ON $database.* TO '$mysqlsiteuser'@'$mysqlhost' IDENTIFIED BY '$mysqlsitepass'",$connect_mysql);
if (!$grant_privileges)
{
  die('Could not grant privileges: ' . mysql_error());
}
$flush_privileges = mysql_query("FLUSH PRIVILEGES",$connect_mysql);
if (!$flush_privileges)
{
  die('Could not flush privileges: ' . mysql_error());
}
mysql_close($connect_mysql);

chdir('/var/www');
exec('drush make ' . $makefile . ' ' . $sitename);
chdir('/var/www/' . $sitename);
exec($drushinstall);
header('Location: http://' . $vhostname . '/' . $sitename);

rmdir('/var/www/magic_hat/rabbit');

?>