#!/bin/sh
inotifywait -m -e create /var/www --format '%f' | while read file; do
  if inotifywait -e delete /var/www/magic_hat; then
    chown -R yourusername:www-data /var/www/$file
  fi
done
